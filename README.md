# DKE-notes
The goal of this repository is to maintain a well organised collection of condensed
LaTeX notes from courses at the Department of Data Science & Knowledge Engineering at
Maastricht University.

Notes are divided into a directory tree based on the periods & years the courses are
in. This should be fairly self-explanatory.

## Contributing
Concisely speaking to contribute you must fork this repository, perform any changes
you wish to see, compile LaTeX files and make a merge request with your updates.

If you have no idea what's going on, below is a quick rundown. Note that you _will
need to_ learn those technologies sooner or later, so you might as well do so now.

### Setup
To perform changes and make them permanent you need to install and learn
[git](https://git-scm.com/).

To edit and compile LaTeX (i.e. `.tex`) files you must find a suitable LaTeX editor.
There are lots of these, so you have a fair bit of freedom. [Here's a fairly sizeable
list of LaTeX
software.](https://tex.stackexchange.com/questions/339/latex-editors-ides/195435)

We, the maintainers, use and love:
- [Gummi](https://github.com/alexandervdm/gummi/wiki/Installing-Gummi)
- vim with [vimtex](https://github.com/lervag/vimtex)

### Modifications
To make modifications you must:
1. [Make a fork of this
   repository.](https://docs.gitlab.com/ee/workflow/forking_workflow.html#creating-a-fork)
2. Perform the changes in `.tex` files and compile all those that you modified into `.pdf`s.
3. `git add` all of the `.tex` and `.pdf` files you have modified.
4. `git commit` with an [informative commit
message](https://chris.beams.io/posts/git-commit/), push the changes to your fork.
5. [Make a merge
   request.](https://docs.gitlab.com/ee/workflow/forking_workflow.html#merging-upstream)

We will then review your changes and either merge them or discuss what we found
problematic for you to fix.
