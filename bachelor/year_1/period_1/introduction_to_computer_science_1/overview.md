# Introduction to Computer Science 1
We have not prepared notes for Intro to CS1 as there are already brilliant notes on
the Incognito wiki. 

Also,  the most important thing to learn from this course is coding itself which
_needs_ to be practiced - you can't turn this into a note.
